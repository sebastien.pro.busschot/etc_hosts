# etc_hosts



## Getting started

```bash
# install
bash etc_hosts.sh install
# Installing makes this actions:
# + chmod +x ./etc_hosts.sh <=========================== NOT REALLY NECESSARY
# + cp ./etc_hosts.sh /home/seb/.local/bin/etc_hosts <== BIN PATH
# + sudo chown -c seb:seb /etc/hosts <================== NEEDS PASSWORD
# + set +x <============================================ SET DEBUG OFF
```
## USE

```bash
# ADD $host to /etc/hosts with 127.0.0.1 IP
etc_host add $host

# ADD $host to /etc/hosts with $ip IP 
etc_host add $host $ip

# REMOVE $host to /etc/hosts
etc_host rm $host

# REMOVE $host to /etc/hosts and REPLACE to same $host with new $ip
etc_host mod $host $ip

# LIST /etc/hosts
etc_host ls
```







