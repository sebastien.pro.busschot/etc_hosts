#!/bin/bash

# Vars
action=$1
host=$2
ip=$3
etc_file=/etc/hosts

# Install
install(){
    echo "Installing makes this actions:"
    set -x
    chmod +x ./etc_hosts.sh
    cp ./etc_hosts.sh ~/.local/bin/etc_hosts
    sudo chown -c $USER:$USER $etc_file
    set +x
}

# Add host
addHost() {
    if [[ "$host" == "" ]]; then
        echo "Host is necessary"
        exit 1
    else
        if [[ "$ip" == "" ]]; then
            ip="127.0.0.1"
        fi
        echo "$ip   $host" >> $etc_file
    fi
    echo "$ip   $host added"
}

# Remove host
rmHost() {
    # Ugly but i dont want to give access to /etc/ for sed
    cp $etc_file /tmp/hosts
    sed -i "/$host/d" /tmp/hosts
    echo "" > $etc_file
    cat /tmp/hosts > $etc_file
    rm /tmp/hosts
    echo "$host removed"
}

# Select action
select_action() {
    if [[ "$action" == "install" ]]; then
        install
    elif [[ "$action" == "add" ]]; then
        addHost
    elif [[ "$action" == "rm" ]]; then
        rmHost
    elif [[ "$action" == "mod" ]]; then
        rmHost
        sleep 0.5
        addHost
    elif [[ "$action" == "ls" ]]; then
        cat $etc_file
    else
        echo "Usage: etc_host install|add|rm|mod|ls [host] [ip]"
        exit 1
    fi
}

select_action
exit 0